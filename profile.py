#!/usr/bin/env python

import geni.portal as portal
from geni.namespaces import Namespace
import geni.rspec.pg as RSpec
import geni.rspec.igext as IG
# Emulab specific extensions.
import geni.rspec.emulab as emulab
from lxml import etree as ET
import crypt
import random
import os.path
import sys

TBCMD = "sudo mkdir -p /root/setup && sudo -H /local/repository/setup-driver.sh 2>&1 | sudo tee /root/setup/setup-driver.log"

#
# For now, disable the testbed's root ssh key service until we can remove ours.
# It seems to race (rarely) with our startup scripts.
#
disableTestbedRootKeys = True

#
# Create our in-memory model of the RSpec -- the resources we're going to request
# in our experiment, and their configuration.
#
rspec = RSpec.Request()

#
# This geni-lib script is designed to run in the CloudLab Portal.
#
pc = portal.Context()

#
# Define *many* parameters; see the help docs in geni-lib to learn how to modify.
#
pc.defineParameter(
    "osNodeType", "Hardware Type",
    portal.ParameterType.NODETYPE, "",
    longDescription="A specific hardware type to use for each node.  Cloudlab clusters all have machines of specific types.  When you set this field to a value that is a specific hardware type, you will only be able to instantiate this profile on clusters with machines of that type.  If unset, when you instantiate the profile, the resulting experiment may have machines of any available type allocated.")

pc.defineParameter(
    "mirrorHost","Ubuntu Package Mirror Hostname",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="A specific Ubuntu package mirror host to use instead of us.archive.ubuntu.com (mirror must have Ubuntu in top-level dir, or you must also edit the mirror path parameter below)")
pc.defineParameter(
    "mirrorPath","Ubuntu Package Mirror Path",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="A specific Ubuntu package mirror path to use instead of /ubuntu/ (you must also set a value for the package mirror parameter)")
pc.defineParameter(
    "doAptDistUpgrade","Upgrade all packages to their latest versions",
    portal.ParameterType.BOOLEAN, False,advanced=True,
    longDescription="If you select this option, we will run an apt-get dist-upgrade to upgrade all packages installed on the node.")

pc.defineParameter(
    "tempBlockstoreMountPoint", "Temporary Filesystem Mount Point",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="Mounts an ephemeral, temporary filesystem at this mount point, on the nodes which you specify below.  If you specify no nodes, and specify a mount point here, all nodes will get a temp filesystem.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).")
pc.defineParameter(
    "tempBlockstoreSize", "Temporary Filesystem Size",
    portal.ParameterType.INTEGER, 0,advanced=True,
    longDescription="The necessary space in GB to reserve for your temporary filesystem.")
pc.defineParameter(
    "tempBlockstoreMountNodes", "Temporary Filesystem Mount Node(s)",
    portal.ParameterType.STRING,"",advanced=True,
    longDescription="The node(s) on which you want a temporary filesystem created; space-separated for more than one.  Leave blank if you want all nodes to have a temp filesystem.")

pc.defineParameter(
    "blockstoreURN", "Remote Dataset URN",
    portal.ParameterType.STRING, "",advanced=True,
    longDescription="The URN of an *existing* remote dataset (a remote block store) that you want attached to the node you specified (defaults to the ctl node).  The block store must exist at the cluster at which you instantiate the profile!")
pc.defineParameter(
    "blockstoreMountNode", "Remote Dataset Mount Node",
    portal.ParameterType.STRING, "ctl",advanced=True,
    longDescription="The node on which you want your remote block store mounted; defaults to the controller node.")
pc.defineParameter(
    "blockstoreMountPoint", "Remote Dataset Mount Point",
    portal.ParameterType.STRING, "/dataset",advanced=True,
    longDescription="The mount point at which you want your remote dataset mounted.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).  Note also that this option requires a network interface, because it creates a link between the dataset and the node where the dataset is available.  Thus, just as for creating extra LANs, you might need to select the Multiplex Flat Networks option, which will also multiplex the blockstore link here.")
pc.defineParameter(
    "blockstoreReadOnly", "Mount Remote Dataset Read-only",
    portal.ParameterType.BOOLEAN, True,advanced=True,
    longDescription="Mount the remote dataset in read-only mode.")

pc.defineParameter(
    "localBlockstoreURN", "Image-backed Dataset URN",
    portal.ParameterType.STRING, "",advanced=True,
    longDescription="The URN of an image-backed dataset that already exists that you want loaded into the node you specified (defaults to the ctl node).  The block store must exist at the cluster at which you instantiate the profile!")
pc.defineParameter(
    "localBlockstoreMountNode", "Image-backed Dataset Mount Node",
    portal.ParameterType.STRING, "ctl",advanced=True,
    longDescription="The node on which you want your image-backed dataset mounted; defaults to the controller node.")
pc.defineParameter(
    "localBlockstoreMountPoint", "Image-Backed Dataset Mount Point",
    portal.ParameterType.STRING, "/image-dataset",advanced=True,
    longDescription="The mount point at which you want your image-backed dataset mounted.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).")
pc.defineParameter(
    "localBlockstoreSize", "Image-Backed Dataset Size",
    portal.ParameterType.INTEGER, 0,advanced=True,
    longDescription="The necessary space to reserve for your image-backed dataset (you should set this to at least the minimum amount of space your image-backed dataset will require).")
pc.defineParameter(
    "localBlockstoreReadOnly", "Mount Image-Backed Dataset Read-only",
    portal.ParameterType.BOOLEAN, True,advanced=True,
    longDescription="Mount the image-backed dataset in read-only mode.")

#
# Get any input parameter values that will override our defaults.
#
params = pc.bindParameters()

#
# Verify our parameters and throw errors.
#


#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

detailedParamAutoDocs = ''
for param in pc._parameterOrder:
    if not pc._parameters.has_key(param):
        continue
    detailedParamAutoDocs += \
      """
  - *%s*

    %s
    (default value: *%s*)
      """ % (pc._parameters[param]['description'],pc._parameters[param]['longDescription'],pc._parameters[param]['defaultValue'])
    pass

tourDescription = \
  "This profile provides one node running HubZero."

tourInstructions = \
  """
### Basic Instructions
Once your experiment node has booted, and this profile's configuration scripts have finished configuring HubZero, you'll be able to visit [the HubZero WWW interface](https://{host-%s}/).  Your HubZero admin password is randomly-generated by Cloudlab, and it is: `{password-adminPass}`

### Detailed Parameter Documentation
%s
""" % ("hz",detailedParamAutoDocs)

#
# Setup the Tour info with the above description and instructions.
#  
tour = IG.Tour()
tour.Description(IG.Tour.TEXT,tourDescription)
tour.Instructions(IG.Tour.MARKDOWN,tourInstructions)
rspec.addTour(tour)

#
# Handle temp blockstore param.  Note that we do not generate errors for
# non-existent nodes!
#
tempBSNodes = []
if params.tempBlockstoreMountPoint != "":
    if params.tempBlockstoreMountNodes:
        tempBSNodes = params.tempBlockstoreMountNodes.split()
    if params.tempBlockstoreSize <= 0:
        perr = portal.ParameterError("Your temporary filesystems must have size > 0!",
                                     ['tempBlockstoreSize'])
        pc.reportError(perr)
        pc.verifyParameters()
    pass

#
# Add the single "hz" node.
#
nodes = {}
node = RSpec.RawPC("hz")
nodes["hz"] = node
if params.osNodeType:
    node.hardware_type = params.osNodeType
node.disk_image = "	urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
node.addService(RSpec.Execute(shell="sh",command=TBCMD))
if disableTestbedRootKeys:
    node.installRootKeys(False, False)
if params.tempBlockstoreMountPoint \
    and (len(tempBSNodes) == 0 or name in tempBSNodes):
    bs = node.Blockstore(
        name + "-temp-bs",params.tempBlockstoreMountPoint)
    bs.size = str(params.tempBlockstoreSize) + "GB"
    bs.placement = "any"

#
# Add the blockstore, if requested.
#
bsnode = None
bslink = None
if params.blockstoreURN != "":
    if not nodes.has_key(params.blockstoreMountNode):
        #
        # This is a very late time to generate a warning, but that's ok!
        #
        perr = portal.ParameterError("The node on which you mount your remote dataset must exist, and does not!",
                                     ['blockstoreMountNode'])
        pc.reportError(perr)
        pc.verifyParameters()
        pass
    
    rbsn = nodes[params.blockstoreMountNode]
    myintf = rbsn.addInterface("ifbs0")
    
    bsnode = IG.RemoteBlockstore("bsnode",params.blockstoreMountPoint)
    bsintf = bsnode.interface
    bsnode.dataset = params.blockstoreURN
    #bsnode.size = params.N
    bsnode.readonly = params.blockstoreReadOnly
    
    bslink = RSpec.Link("bslink")
    bslink.addInterface(myintf)
    bslink.addInterface(bsintf)
    # Special blockstore attributes for this link.
    bslink.best_effort = True
    bslink.vlan_tagging = True
    pass

#
# Add the local blockstore, if requested.
#
lbsnode = None
if params.localBlockstoreURN != "":
    if not nodes.has_key(params.localBlockstoreMountNode):
        #
        # This is a very late time to generate a warning, but that's ok!
        #
        perr = portal.ParameterError("The node on which you mount your image-backed dataset must exist, and does not!",
                                     ['localBlockstoreMountNode'])
        pc.reportError(perr)
        pc.verifyParameters()
        pass
    if params.localBlockstoreSize is None or params.localBlockstoreSize <= 0 \
      or str(params.localBlockstoreSize) == "":
        #
        # This is a very late time to generate a warning, but that's ok!
        #
        perr = portal.ParameterError("You must specify a size (> 0) for your image-backed dataset!",
                                     ['localBlockstoreSize'])
        pc.reportError(perr)
        pc.verifyParameters()
        pass

    lbsn = nodes[params.localBlockstoreMountNode]
    lbsnode = lbsn.Blockstore("lbsnode",params.localBlockstoreMountPoint)
    lbsnode.dataset = params.localBlockstoreURN
    lbsnode.size = str(params.localBlockstoreSize)
    lbsnode.readonly = params.localBlockstoreReadOnly
    pass

for nname in nodes.keys():
    rspec.addResource(nodes[nname])
if bsnode:
    rspec.addResource(bsnode)
if bslink:
    rspec.addResource(bslink)
    pass

class EmulabEncrypt(RSpec.Resource):
    def _write(self, root):
        ns = "{http://www.protogeni.net/resources/rspec/ext/emulab/1}"

#        el = ET.SubElement(root,"%sencrypt" % (ns,),attrib={'name':'adminPass'})
#        el.text = params.adminPass
        el = ET.SubElement(root,"%spassword" % (ns,),attrib={'name':'adminPass'})
        pass
    pass

stuffToEncrypt = EmulabEncrypt()
rspec.addResource(stuffToEncrypt)

pc.printRequestRSpec(rspec)
