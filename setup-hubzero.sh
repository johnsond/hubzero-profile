#!/bin/sh

set -x
set -e

if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

if [ -f $OURDIR/hubzero-done ]; then
    exit 0
fi

logtstart "hubzero"

if [ -f $SETTINGS ]; then
    . $SETTINGS
fi
if [ -f $LOCALSETTINGS ]; then
    . $LOCALSETTINGS
fi

fh=`cat /etc/hostname`
if [ ! $fh = `hostname` ]; then
    hostname > /etc/hostname
fi

if [ ! -f /etc/apt/sources.list.d/hubzero.list ]; then
    hostname > /etc/hostname
    apt-key adv --keyserver keys.gnupg.net --recv-keys 143C99EF
    echo "deb http://packages.hubzero.org/deb julian-deb8 main" \
	> /etc/apt/sources.list.d/hubzero.list
    apt-get -y update
fi

maybe_install_packages dirmngr software-properties-common
maybe_install_packages hubzero-iptables-basic
systemctl enable hubzero-iptables-basic
systemctl start hubzero-iptables-basic
maybe_install_packages hubzero-mysql
systemctl enable mysql && systemctl start mysql
maybe_install_packages dma mailutils
cp /etc/hostname /etc/mailname
maybe_install_packages hubzero-apache2
a2dissite 000-default default-ssl
maybe_install_packages hubzero-php
maybe_install_packages hubzero-cms-2.2
hzcms install example && hzcms update
a2ensite example example-ssl
systemctl enable apache2 && systemctl restart apache2

# Skip the intro page (https://pc830.emulab.net/gettingstarted)
wget "https://pc830.emulab.net/?getstarted=1"

maybe_install_packages hubzero-mailgateway
hzcms configure mailgateway --enable

# Preseed the debconf cache with our answers so we don't have to do any
# manual config afterward.
cat <<EOF >>/root/ldap-debconf-config.dat
Name: nslcd/ldap-base
Template: nslcd/ldap-base
Value: dc=example,dc=com
Owners: nslcd

Name: nslcd/ldap-uris
Template: nslcd/ldap-uris
Value: ldap://127.0.0.1/
Owners: nslcd

Name: libnss-ldapd/nsswitch
Template: libnss-ldapd/nsswitch
Value: group, passwd, shadow
Owners: libnss-ldapd, libnss-ldapd:amd64

Name: slapd/internal/adminpw
Template: slapd/internal/adminpw
Value: foobar
Owners: slapd
EOF

export DEBCONF_DB_OVERRIDE=/root/ldap-debconf-config.dat
maybe_install_packages hubzero-openldap
export DEBCONF_DB_OVERRIDE=
systemctl restart slapd
hzldap init dc=example,dc=com
hzcms configure ldap --enable
hzldap syncusers

getent passwd | grep -q admin.\*1000
if [ ! $? -eq 0 ] ; then
    echo "WARNING: ldap not functioning properly; continuing anyway!"
fi

maybe_install_packages hubzero-webdav cadaver
hzcms configure webdav --enable
ls -l /webdav/home/admin
if [ ! $? -eq 0 ]; then
    echo "WARNING: webdav dirs not functioning properly; continuing anyway!"
fi
#cat <<EOF >>/root/.netrc
#machine localhost
#login admin
#passwd foobar
#EOF
#cadaver https://localhost/webdav

maybe_install_packages hubzero-subversion
hzcms configure subversion --enable
maybe_install_packages hubzero-trac
hzcms configure trac --enable
maybe_install_packages hubzero-forge
hzcms configure forge --enable

# We are not going to run a 2.6.32 kernel!
if false ; then
    maybe_install_packages hubzero-openvz-repo
    apt-get update -y
    maybe_install_packages hubzero-openvz
    hzcms configure openvz --enable
    reboot
fi

maybe_install_packages hubzero-telequotad
systemctl enable telequotad && systemctl start telequotad
sudo mount -oremount /
systemctl enable quota && systemctl restart quota
sudo hzcms configure telequotad --enable
repquota -a

maybe_install_packages hubzero-app hubzero-app-workspace
hubzeroapp install --publish /usr/share/hubzero/apps/workspace-1.3.hza

maybe_install_packages hubzero-filexfer-xlate
hzcms configure filexfer --enable

maybe_install_packages hubzero-submit-pegasus hubzero-submit-condor \
    hubzero-submit-common hubzero-submit-server hubzero-submit-distributor \
    hubzero-submit-monitors
hzcms configure submit-server --enable
systemctl enable submit-server && systemctl start submit-server

touch $OURDIR/hubzero-done

logtend "hubzero"

exit 0
